import React from 'react';
import { 
    Grid,
    CssBaseline,
    Container,
    Paper, 
    Typography,
    TextField,
    FormControl,
    InputLabel,
    OutlinedInput,
    InputAdornment,
    IconButton,
    Button 
} from '@material-ui/core';

import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import AccountCircle from '@material-ui/icons/AccountCircle';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';

import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        paddingTop: '7rem'
    },
    customFont: {
        fontSize: '12px',
        textDecoration: 'none'
    },
    paper: {
        height: '20rem',
        padding: '1rem',
    },
    logo: {
        borderRadius: '50px',
        background: '#dadada',
        width: '60px',
        height: '60px',
        textAlign: 'center',
        lineHeight: '2.5'
    },
    link: {
        textDecoration: 'none',
        color: '#fff'
    },
    footer: {
        background: '#3F51B5', 
        borderRadius: '10px', 
        padding: '1rem 2rem', 
        color: '#fff',
        marginTop: '10rem'
    },
    iconColor: {
        color: '#0000008a'
    }
}));

function LoginPage(props) {
    const classes = useStyles();

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="xl">
                <Grid 
                    container
                    justify="center"
                    alignItems="center"
                    className={classes.root}
                >
                    <Grid 
                        item 
                        xs={12} 
                        lg={3}
                    >
                        <Paper className={classes.paper}>
                            <Grid container justify="center">
                                <Grid item xs={12} align="center" style={{ marginBottom: '1.5rem' }}>
                                    <div className={classes.logo}>
                                        <h1 style={{ color: 'gray' }}>C</h1>
                                    </div>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl 
                                        //className={clsx(classes.margin, classes.textField)} 
                                        variant="outlined"
                                        fullWidth={true}
                                        size="small"
                                        style={{ margin: '.5rem 0rem' }}
                                        >
                                        <InputLabel htmlFor="outlined-adornment-password">Username</InputLabel>
                                        <OutlinedInput
                                            //id="outlined-adornment-password"
                                            startAdornment={
                                                <InputAdornment position="start">
                                                    <PersonIcon 
                                                        className={classes.iconColor}
                                                    />
                                                </InputAdornment>
                                            }
                                            //type={values.showPassword ? 'text' : 'password'}
                                            //value={values.password}
                                            //onChange={handleChange('password')}
                                            labelWidth={70}
                                        />
                                    </FormControl>
                                    <FormControl 
                                        //className={clsx(classes.margin, classes.textField)} 
                                        variant="outlined"
                                        fullWidth={true}
                                        size="small"
                                        style={{ margin: '.5rem 0rem' }}
                                        >
                                        <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                        <OutlinedInput
                                            id="outlined-adornment-password"
                                            //type={values.showPassword ? 'text' : 'password'}
                                            //value={values.password}
                                            //onChange={handleChange('password')}
                                            startAdornment={
                                                <InputAdornment position="start">
                                                    <LockIcon 
                                                        className={classes.iconColor}
                                                    />
                                                </InputAdornment>
                                            }
                                            endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    style={{ padding: '6px' }}
                                                    aria-label="toggle password visibility"
                                                    //onClick={handleClickShowPassword}
                                                    //onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    <Visibility />
                                                {/*values.showPassword ? <Visibility /> : <VisibilityOff />*/}
                                                </IconButton>
                                            </InputAdornment>
                                            }
                                            labelWidth={70}
                                        />
                                    </FormControl>
                                    <Button 
                                        onClick={ () => props.history.push('/') }
                                    style={{ marginTop: '1rem' }} variant="contained" color="primary" fullWidth={true} disableElevation>
                                        Login
                                    </Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper
                            className={classes.footer}
                        >
                            <Grid container justify="flex-end">
                                <Grid style={{ marginRight: '3rem' }}>
                                        <Typography
                                            className={classes.customFont}
                                        >
                                            <span 
                                                className="material-icons"
                                                style={{ fontSize: '12px', verticalAlign: 'text-top', marginRight: '.5rem', color: '#3CEEA3' }}
                                            >
                                                fiber_manual_record
                                            </span>
                                            Status
                                        </Typography>
                                </Grid>
                                <Grid>
                                    <Link className={classes.link}>
                                        <Typography
                                            className={classes.customFont}
                                        >
                                            Contact Support
                                        </Typography>
                                    </Link>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        </React.Fragment>
    );
}

export default LoginPage;
