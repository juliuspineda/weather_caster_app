import React from 'react';
import { Route, Switch } from "react-router-dom";
import DashboardPage from '../DashboardPage/DashboardPage';
import WeatherDashboardPage from '../_WeatherDashboard/WeatherDashboardPage';
import LoginPage from '../LoginPage/LoginPage';
import MainNavigation from './Section/MainNavigation';

function Navigation(props) {   
    return (
        <div>   
            <MainNavigation history={props.history}>
                <main>
                    <Switch>
                        <Route exact path="/" render={ props => <DashboardPage {...props}/> }/>    
                        <Route exact path="/dashboard" render={ props => <WeatherDashboardPage {...props}/> }/>    
                        <Route exact path="/login" component={LoginPage}/>
                        <Route exact path="/rules-engine" component={LoginPage}/>
                        <Route exact path="/notification-engine" component={LoginPage}/>
                        <Route exact path="/user-management" component={LoginPage}/>
                        <Route exact path="/weather-data-management" component={LoginPage}/>
                        <Route exact path="/account-settings" component={LoginPage}/>
                    </Switch>
                </main>
            </MainNavigation>
        </div>
    )
}

export default Navigation;
