import React, {useState, Fragment} from 'react'
import {makeStyles,withStyles,useTheme} from '@material-ui/core/styles';
import clsx from 'clsx';

import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    CssBaseline,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    Grid,
    Avatar,
    Badge,
    IconButton,
    Paper,
    Chip,
    FormControl,
    OutlinedInput,
    InputAdornment
} from '@material-ui/core';

import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import TextFieldsOutlinedIcon from '@material-ui/icons/TextFieldsOutlined';
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
import DashboardPage from '../../DashboardPage/DashboardPage';

const iconImg = require('../../../../assets/images/CASTER.png');
const avatarImg = require('../../../../assets/images/img_avatar.png');

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
      },
      appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      menuButton: {
        marginRight: 30,
      },
      hide: {
        display: 'none',
      },
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
        //whiteSpace: 'nowrap',
      },
      drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen,
        }),
      },
      drawerClose: {
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
        //   width: theme.spacing(9) + 1,
        },
      },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        minHeight: '30px',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    dashboardPaper: {
        borderRadius: '5px',
        padding: '10px'
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    medium: {
        width: '70px',
        height: '70px',
    },
    smallIcon: {
        width: '25px',
        height: '25px',
    },
    defaultColor: {
        color: '#fff'
    },
    searchContainer: {
        marginTop: '.5rem'
    },
    icons: {
        width: '23px',
        height: '23px'
    },
    searchInput: {
        background: '#fff'
    },
    contentPaper: {
        height: '300px',
        padding: theme.spacing(2)
    }
}));

// styled-components
const StyledListItem = withStyles((theme) => ({
    button: {
        '&:hover': {
            borderRight: '3px solid #3F51B5',
            borderRadius: '4px',
            color: '#3F51B5'

        },
        '&:selected': {
            color: '#3F51B5'
        }
    }
}))(ListItem);

const StyledBadge = withStyles((theme) => ({
    badge: {
      backgroundColor: '#44b700',
      color: '#44b700',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
}))(Badge);

function MainNavigation(props) {
    console.log('props main', props.history.location.pathname)
    const classes = useStyles();
    const theme = useTheme();

    const [open, setOpen] = useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    console.log('main',props.children)

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar 
                position="fixed" 
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar style={{ minHeight: '50px' }}>
                    <Grid item lg={6} md={6} sm={6} xs={6}>
                        <div style={{display:'flex'}}>
                        <div style={{alignSelf: 'center'}}>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerOpen}
                                edge="start"
                                className={clsx(classes.menuButton, {
                                [classes.hide]: open,
                                })}
                            >
                                <MenuIcon />
                            </IconButton>
                        </div>
                        <div>
                            <Avatar alt="Caster" src={iconImg} className={classes.medium}/>
                        </div>
                        </div>
                    </Grid>
                    <Grid item lg={6} md={6} sm={6} xs={6} style={{ textAlign: 'end' }}>
                    <Button 
                        onClick={ () => props.history.push('/login')}
                    disableElevation>
                        <ExitToAppOutlinedIcon className={clsx(classes.smallIcon, classes.defaultColor)}/>
                    </Button>
                    </Grid>
                    
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                  })}
                  classes={{
                    paper: clsx({
                      [classes.drawerOpen]: open,
                      [classes.drawerClose]: !open,
                    }),
                  }}
            >
                <Grid>
                    <Grid className={classes.toolbar}>
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                        </IconButton>
                    </Grid>
                    <Grid align="center">
                        {
                            open && <Fragment>
                                <StyledBadge
                                    overlap="circle"
                                    anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                    }}
                                    variant="dot"
                                >
                                    <Avatar alt="JC Torreda" src={avatarImg} className={classes.large} />
                                </StyledBadge>
                                    <Typography variant="h6" style={{ marginTop: '.75rem' }}>JC Torreda</Typography>
                                    <Typography variant="body2">SUPER ADMIN</Typography>
                                <div className={classes.toolbar}/>
                            </Fragment>
                        }
                    </Grid>
                </Grid>
                {
                    (open == true) ? <Divider /> : <div style={{marginTop: '1rem'}}/>
                }
                <List>
                {['Dashboard', 'Rules Engine', 'Notification Engine', 'User Management', 'Weather Data Management', 'Account Settings'].map((text, index) => (
                    <StyledListItem button key={text}>
                        <ListItemIcon>{
                            (index === 0) ? 
                                <DashboardOutlinedIcon onClick={ () => props.history.push('/') }/> 
                            : 
                            (index === 1) ? 
                                <LockOpenIcon onClick={ () => props.history.push('/dashboard') }/>
                            :
                            (index === 2) ? 
                                <NotificationsNoneIcon />
                            : 
                            (index === 3) ? 
                                <PeopleAltOutlinedIcon />
                            : 
                            (index === 4) ? 
                                <TextFieldsOutlinedIcon />
                            :  
                            <AccountTreeOutlinedIcon />
                            }
                        </ListItemIcon>
                    <ListItemText primary={text} />
                    </StyledListItem>
                ))}
                </List>
                <Divider />
                <List>
                {/* {['All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text}>
                    <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                    <ListItemText primary={text} />
                    </ListItem>
                ))} */}
                {
                    <StyledListItem button>
                        <ListItemIcon>
                            <ContactSupportIcon />
                        </ListItemIcon>
                        <ListItemText primary={'Support'}/>
                    </StyledListItem>
                }
                </List>
            </Drawer>
            <div style={{width: '100%'}}>
                {
                   props.children
                }
            </div>
        </div>
    )
}

export default MainNavigation;
