import React from 'react'
import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    CssBaseline,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    Grid,
    Avatar,
    Badge,
    IconButton,
    Paper,
    Chip,
    FormControl,
    OutlinedInput,
    InputAdornment
} from '@material-ui/core';

import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import TextFieldsOutlinedIcon from '@material-ui/icons/TextFieldsOutlined';
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import CachedOutlinedIcon from '@material-ui/icons/CachedOutlined';
import FilterListOutlinedIcon from '@material-ui/icons/FilterListOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import ArrowDropDownOutlinedIcon from '@material-ui/icons/ArrowDropDownOutlined';

import Pagination from '@material-ui/lab/Pagination';

import {makeStyles,withStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import moment from 'moment';

// parent styles
const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        boxShadow: 'none'
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: {
        minHeight: '30px'
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    dashboardPaper: {
        borderRadius: '5px',
        padding: '10px'
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    medium: {
        width: '70px',
        height: '70px',
    },
    smallIcon: {
        width: '25px',
        height: '25px',
    },
    defaultColor: {
        color: '#fff'
    },
    searchContainer: {
        marginTop: '.5rem'
    },
    icons: {
        width: '23px',
        height: '23px'
    },
    searchInput: {
        background: '#fff'
    },
    contentPaper: {
        height: '300px',
        padding: theme.spacing(2)
    }
}));

// styled-components
const StyledListItem = withStyles((theme) => ({
    button: {
        '&:hover': {
            borderRight: '3px solid #3F51B5',
            borderRadius: '4px',
            color: '#3F51B5'

        },
        '&:selected': {
            color: '#3F51B5'
        }
    }
}))(ListItem);

const StyledButton = withStyles((theme) => ({
    root: {
        fontSize: '10px'
    },
    contained: {
        background: '#fff',
        '&:hover': {
            background: '#3F51B5',
            color: "#fff"
        },
        marginRight: '.5rem'
    }
}))(Button);

const StyledBadge = withStyles((theme) => ({
    badge: {
      backgroundColor: '#44b700',
      color: '#44b700',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
}))(Badge);

function DashboardPage(props) {
    console.log('asdasda',props)
    const classes = useStyles();

    let timeNow = moment().format('h:mm A');
    let dateNow = moment().format('dddd, MMMM D, YYYY');

    return (
        <div>
            <main className={classes.content}>
                <div style={{ minHeight: '72px' }}/>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                        <Paper className={classes.dashboardPaper} fullWidth>
                            <Grid container>
                                <Grid item lg={6} md={6} sm={6} xs={6}>
                                    <Typography variant="h6"><DashboardOutlinedIcon style={{ verticalAlign: 'sub', marginRight: '.5rem', color: '#3F51B5' }}/>Dashboard</Typography>
                                </Grid>
                                <Grid item lg={6} md={6} sm={6} xs={6} style={{ textAlign: 'end' }}>
                                    <Grid container justify="flex-end">
                                        <Typography variant="p" style={{alignSelf: 'center',marginRight: '.5rem'}}>Updated as of</Typography>
                                        <Chip label={timeNow} 
                                            style={{ fontSize: '1rem', background: 'transparent', border: '1px solid #e0e0e0', height: '30px', marginRight: '.5rem' }}
                                        />
                                        
                                        <Typography variant="h6" style={{alignSelf: 'center'}}>{dateNow}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} className={classes.searchContainer}>
                        <StyledButton variant="contained"><CachedOutlinedIcon className={classes.icons}/></StyledButton>
                        <StyledButton variant="contained">Filter<FilterListOutlinedIcon className={classes.icons} style={{ marginLeft: '.5rem' }}/></StyledButton>
                        <FormControl 
                            //className={clsx(classes.margin, classes.textField)} 
                            variant="outlined"
                            style={{ width: '30%' }}
                            size="small"
                            >
                            <OutlinedInput
                                //id="outlined-adornment-password"
                                //type={values.showPassword ? 'text' : 'password'}
                                //value={values.password}
                                //onChange={handleChange('password')}
                                placeholder="Search By Admin Name, ID, Location"
                                className={classes.searchInput}
                                autoFocus
                                startAdornment={
                                    <InputAdornment position="start">
                                        <SearchOutlinedIcon className={classes.icons}/>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} className={classes.searchContainer}>
                        <Grid container spacing={2}>
                            {
                                ["Pasig","Quezon","Manila","Paranaque","Quezon","Makati"].map((item,key) => <Grid key={key} item lg={4}>
                                <Paper className={classes.contentPaper}>
                                    <Grid container>
                                        <Grid item lg={12} align="center">
                                            <Typography variant="h5">{item + ' City' }</Typography>
                                            <Typography variant="p" style={{fontSize: '11px'}}>{ 'Location ID: 00000' + (key + 1) }</Typography>
                                        </Grid>
                                        <Grid container item lg={12} align="center" style={{marginTop:'.5rem'}}>
                                            <Grid item lg={6} style={{ height: '70px', width: '50%' }}>
                                                <div>
                                                    asdsad
                                                </div>
                                            </Grid>
                                            <Grid item lg={6} style={{ height: '70px', width: '50%' }}>
                                                <Grid><Typography variant="p" style={{fontSize: '14px',fontWeight: '900'}}>Partly Cloud</Typography></Grid>
                                                <Typography variant="p" style={{fontSize: '10px',fontWeight: '900'}}>{ '32%' + ' Chances of rain' }</Typography>
                                            </Grid>
                                            <Grid alignItems="center" item lg={6}>
                                                <Typography variant="p" style={{fontSize: '10px',fontWeight: '900'}}>ACTIVE USERS</Typography>
                                                <Typography variant="h5">2,150</Typography>
                                                <Typography variant="p" style={{fontSize: '10px',lineHeight: '1'}}>
                                                    <ArrowDropDownOutlinedIcon
                                                        style={{verticalAlign: 'bottom'}}
                                                    />
                                                    { '12% ' + ' Since last month' }
                                                </Typography>
                                            </Grid>
                                            <Grid alignItems="center" item lg={6} style={{ height: '80px', width: '50%' }}>
                                                <Typography variant="p" style={{fontSize: '10px',fontWeight: '900'}}>ALERT SENT</Typography>
                                                <Typography variant="h5">600</Typography>
                                                <Typography variant="p" style={{fontSize: '10px',lineHeight: '1'}}>
                                                    <ArrowDropDownOutlinedIcon
                                                        style={{verticalAlign: 'bottom'}}
                                                    />
                                                    { '12% ' + ' Since last month' }
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid container item lg={12} align="center">
                                            <Grid alignItems="center" item lg={6} style={{ height: '80px', width: '50%' }}>
                                                <Typography variant="p" style={{fontSize: '10px',fontWeight: '900'}}>NOTIFICATION SENT</Typography>
                                                <Typography variant="h5">2,150</Typography>
                                                <Typography variant="p" style={{fontSize: '10px',lineHeight: '1'}}>
                                                    <ArrowDropDownOutlinedIcon
                                                        style={{verticalAlign: 'bottom'}}
                                                    />
                                                    { '12% ' + ' Since last month' }
                                                </Typography>
                                            </Grid>
                                            <Grid alignItems="center" item lg={6} style={{ height: '80px', width: '50%' }}>
                                                <Typography variant="p" style={{fontSize: '10px',fontWeight: '900'}}>BROADCAST MESSAGE</Typography>
                                                <Typography variant="h5">1200</Typography>
                                                <Typography variant="p" style={{fontSize: '10px',lineHeight: '1'}}>
                                                    <ArrowDropDownOutlinedIcon
                                                        style={{verticalAlign: 'bottom'}}
                                                    />
                                                    { '12% ' + ' Since last month' }
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>)
                            }
                        </Grid>
                        <Grid container justify="center" style={{ marginTop: '1rem' }}>
                            <Pagination count={10} variant="outlined" shape="rounded" style={{background: '#fff'}}/>
                        </Grid>
                    </Grid>
                </Grid>
            </main>
        </div>
    );
}

export default DashboardPage;
