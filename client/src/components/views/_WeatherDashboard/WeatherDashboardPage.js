import React from 'react'
import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    CssBaseline,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider,
    Grid,
    Avatar,
    Badge,
    IconButton,
    Paper,
    Chip,
    FormControl,
    OutlinedInput,
    InputAdornment
} from '@material-ui/core';

import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import TextFieldsOutlinedIcon from '@material-ui/icons/TextFieldsOutlined';
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import CachedOutlinedIcon from '@material-ui/icons/CachedOutlined';
import FilterListOutlinedIcon from '@material-ui/icons/FilterListOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import ArrowDropDownOutlinedIcon from '@material-ui/icons/ArrowDropDownOutlined';

import Pagination from '@material-ui/lab/Pagination';

import {makeStyles,withStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import moment from 'moment';

// parent styles
const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        boxShadow: 'none'
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: {
        minHeight: '30px'
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(1.5),
    },
    dashboardPaper: {
        borderRadius: '5px',
        padding: '3px'
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    medium: {
        width: '70px',
        height: '70px',
    },
    smallIcon: {
        width: '25px',
        height: '25px',
    },
    defaultColor: {
        color: '#fff'
    },
    searchContainer: {
        marginTop: '.5rem'
    },
    icons: {
        width: '23px',
        height: '23px'
    },
    searchInput: {
        background: '#fff'
    },
    contentPaper: {
        height: '300px',
        padding: theme.spacing(2)
    }
}));

// styled-components
const StyledListItem = withStyles((theme) => ({
    button: {
        '&:hover': {
            borderRight: '3px solid #3F51B5',
            borderRadius: '4px',
            color: '#3F51B5'

        },
        '&:selected': {
            color: '#3F51B5'
        }
    }
}))(ListItem);

const StyledButton = withStyles((theme) => ({
    root: {
        fontSize: '10px'
    },
    contained: {
        background: '#fff',
        '&:hover': {
            background: '#3F51B5',
            color: "#fff"
        },
        marginRight: '.5rem'
    }
}))(Button);

const StyledBadge = withStyles((theme) => ({
    badge: {
      backgroundColor: '#44b700',
      color: '#44b700',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
}))(Badge);

function WeatherDashboardPage(props) {
    const classes = useStyles();

    let timeNow = moment().format('h:mm A');
    let dateNow = moment().format('dddd, MMMM D, YYYY');

    return (
        <div className={classes.content}>
            <div style={{ minHeight: '72px' }}/>
            <Grid container>
                <Grid xs={12} sm={12} md={12} lg={12}>
                    <div style={{ display: 'flex' }}>
                        <div style={{ width: '12%' }}>
                            <StyledButton variant="contained"><CachedOutlinedIcon className={classes.icons}/></StyledButton>
                            <StyledButton variant="contained"><CachedOutlinedIcon className={classes.icons}/></StyledButton>
                        </div>
                        <div style={{ width: '89%' }}>
                            <Paper className={classes.dashboardPaper} fullWidth>
                                <Grid container>
                                    <Grid container item lg={6} md={6} sm={6} xs={6}>
                                        <DashboardOutlinedIcon style={{ alignSelf: 'center', marginRight: '.5rem', marginLeft: '.5rem', color: '#3F51B5' }}/>   
                                        <Typography variant="h6" style={{ alignSelf: 'center' }}>Dashboard</Typography>
                                    </Grid>
                                    <Grid item lg={6} md={6} sm={6} xs={6} style={{ textAlign: 'end' }}>
                                        <Grid container justify="flex-end">
                                            <Typography variant="p" style={{alignSelf: 'center',marginRight: '.5rem'}}>Updated as of</Typography>
                                            <Chip label={timeNow} 
                                                style={{ fontSize: '1rem', background: 'transparent', border: '1px solid #e0e0e0', height: '30px', marginRight: '.5rem' }}
                                            />
                                            
                                            <Button variant="outlined" style={{ padding: '3px 15px', marginRight: '.5rem' }}>Share this dashboard</Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </div>
                    </div>
                    <Grid item xs={12} sm={12} md={12} lg={12} style={{ marginTop: '.5rem' }}>
                        <Grid container spacing={1}>
                            <Grid item xs={2} sm={2} md={2} lg={2}>
                                <Grid container>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                        <Paper style={{ height: '400px' }}>
                                                        asdasd
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12} lg={12} style={{ marginTop: '.5rem' }}>
                                        <Paper style={{ height: '150px' }}>
                                                        asdasd
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12} lg={12} style={{ marginTop: '.5rem' }}>
                                        <Grid container spacing={1}>
                                            <Grid item xs={6} sm={6} md={6} lg={6}>
                                                <Paper style={{ height: '100px' }}>
                                                    sadsad
                                                </Paper>
                                            </Grid>
                                            <Grid item xs={6} sm={6} md={6} lg={6}>
                                                <Paper style={{ height: '100px' }}>
                                                    sadsad
                                                </Paper>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={6} sm={6} md={6} lg={6}>
                                <Grid container>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                        <Paper style={{ height: '100px' }}>
                                                            asdasd
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12} lg={12} style={{ marginTop: '.5rem' }}>
                                        <Grid container spacing={1}>
                                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                                <Paper style={{ height: '100px' }}>
                                                                    asdasd
                                                </Paper>
                                            </Grid>
                                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                                <Paper style={{ height: '100px' }}>
                                                                    asdasd
                                                </Paper>
                                            </Grid>
                                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                                <Paper style={{ height: '100px' }}>
                                                                    asdasd
                                                </Paper>
                                            </Grid>
                                            <Grid item xs={3} sm={3} md={3} lg={3}>
                                                <Paper style={{ height: '100px' }}>
                                                                    asdasd
                                                </Paper>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12} lg={12} style={{ marginTop: '.5rem' }}>
                                        <Grid container spacing={1}>
                                            <Grid item xs={7} sm={7} md={7} lg={7}>
                                                <Paper style={{ height: '450px' }}>
                                                        sad
                                                </Paper>
                                            </Grid>
                                            <Grid item xs={5} sm={5} md={5} lg={5}>
                                                <Grid spacing={1}>
                                                    <Grid>
                                                        <Paper style={{ height: '106.5px' }}>
                                                                asds
                                                        </Paper>
                                                    </Grid>
                                                    <Grid style={{ marginTop: '.5rem' }}>
                                                        <Paper style={{ height: '106.5px' }}>
                                                                asds
                                                        </Paper>
                                                    </Grid>
                                                    <Grid style={{ marginTop: '.5rem' }}>
                                                        <Paper style={{ height: '106.5px' }}>
                                                                asds
                                                        </Paper>
                                                    </Grid>
                                                    <Grid style={{ marginTop: '.5rem' }}>
                                                        <Paper style={{ height: '106.5px' }}>
                                                                asds
                                                        </Paper>
                                                    </Grid>
                                                </Grid>
                                                
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={4} sm={4} md={4} lg={4}>
                                <Grid container spacing={1}>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                        <Paper style={{ height: '400px' }}>
                                            asdasd
                                        </Paper> 
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                        <Paper style={{ height: '258px' }}>
                                            asdasd
                                        </Paper> 
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default WeatherDashboardPage;
