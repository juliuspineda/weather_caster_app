import React, { Suspense, useState } from 'react';
import { Route, Switch } from "react-router-dom";

import Navigation from './views/_Navigation/Navigation';
import LoginPage from './views/LoginPage/LoginPage';

import { createMuiTheme,ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
      fontSize: 12
    }
});

function App() {

    const [isLogin, setLogin] = useState(false);

    return (
        <Suspense fallback={(<div>Loading...</div>)}>
            <div style={{ minHeight: '100vh' }}>
                <ThemeProvider theme={theme}>
                    <Switch>
                        <Route path="/" render={ props => {
                           if(isLogin == false) {
                                return <Navigation {...props} />
                           }
                                return <LoginPage {...props} />
                        }}/>
                    </Switch>
                </ThemeProvider>
            </div>
        </Suspense>
    );
}

export default App;
